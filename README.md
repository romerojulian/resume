# My Resume

[Preview Resume](https://romerojulian.gitlab.io/resume/)

## Download

You can download my resume in two ([ats compatible](https://en.wikipedia.org/wiki/Applicant_tracking_system)) formats

* [PDF](https://gitlab.com/romerojulian/resume/raw/master/public/cv-romerojulian.pdf)
* [DOCX](https://gitlab.com/romerojulian/resume/raw/master/public/cv-romerojulian.docx)


### Credits

[Tim Baker](https://github.com/tbakerx) and his [react resume template](https://github.com/tbakerx/react-resume-template)
