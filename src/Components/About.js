import React from 'react';

const About = ({ data }) => {
  if (!data) return null;

  const { bio, atsresumepdf, atsresumedocx, name, address, email } = data;

  return (
    <section id='about'>
      <div className='row'>
        <div className='three columns'>
          <div className='profile-pic' alt='Julián Romero Profile Pic' />
        </div>
        <div className='nine columns main-col'>
          <h2>About Me</h2>
          <p>{bio}</p>
          <div className='row'>
            <div className='columns contact-details'>
              <h2>Contact Details</h2>
              <p className='address'>
                <span>{name}</span><br />
                <span>{address.street}, {address.city} - {address.state}
                </span><br />
                <span>{email}</span>
              </p>
            </div>
            <div className='columns download'>
              <p>
                <a href={atsresumepdf} className='button' target='_blank' rel='noreferrer'>
                  <i className='fa fa-download' />
                  PDF Resume
                </a>
                <a href={atsresumedocx} className='button'>
                  <i className='fa fa-download' />
                  DOCX Resume
                </a>
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default About;
