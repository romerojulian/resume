import React from 'react';
import styled from 'styled-components';

const StyledLinkedin = styled.a`
  color: #fff !important;
  font-size: 20px;

  &:hover {
    color: #11ABB0 !important;
  }
`

const Testimonials = ({ data }) => {
  if (!data) return null;

  const { testimonials } = data;

  return (
    <section id='testimonials'>
      <div className='text-container'>
        <div className='row'>
          <div className='two columns header-col'>
            <h1>
              <span>Client Testimonials</span>
            </h1>
          </div>

          <div className='ten columns flex-container'>
            <ul className='slides'>
              {testimonials.map((testimonials) => {
                return (
                  <li key={testimonials.user}>
                    <blockquote>
                      <p>{testimonials.text}</p>
                      <cite>
                        {' '}{testimonials.user}
                        <StyledLinkedin href={`https://www.linkedin.com/in/${testimonials.linkedin}`} target='_blank' rel='noreferrer'>
                          {' '}<i className='fa fa-linkedin' />
                        </StyledLinkedin>
                      </cite>
                    </blockquote>
                  </li>
                );
              })}
            </ul>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Testimonials;
